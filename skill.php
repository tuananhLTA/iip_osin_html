
<?php
include "header.php";
?>

<main>
	<section class="site-news-page">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="site-news-page_content">
						<div class="title">
							<h2 class="heading"><a href="#" title="">Quy định pháp luật</a></h2>
						</div>
						<div class="title-page">
							<h1>An toàn lao động </h1>
						</div>
						<div class="site-news-page_content_list">
							<div class="row">
								<div class="col-md-6">
									<div class="blog">
										<a class="overflow" href="#" title="">
											<img class="w-100" src="assets/images/n1.png" alt="">
										</a>
										<h3><a href="#" title="">HENDRERIT NISI VENENATIS</a></h3>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
												<div class="list-mxh">
													<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
													<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
													<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
												</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="blog">
										<a class="overflow" href="#" title="">
											<img class="w-100" src="assets/images/n2.png" alt="">
										</a>
										<h3><a href="#" title="">HENDRERIT NISI VENENATIS</a></h3>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
												<div class="list-mxh">
													<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
													<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
													<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
												</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="blog">
										<a class="overflow" href="#" title="">
											<img class="w-100" src="assets/images/n3.png" alt="">
										</a>
										<h3><a href="#" title="">HENDRERIT NISI VENENATIS</a></h3>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
												<div class="list-mxh">
													<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
													<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
													<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
												</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="blog">
										<a class="overflow" href="#" title="">
											<img class="w-100" src="assets/images/n4.png" alt="">
										</a>
										<h3><a href="#" title="">HENDRERIT NISI VENENATIS</a></h3>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
												<div class="list-mxh">
													<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
													<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
													<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
												</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="blog">
										<a class="overflow" href="#" title="">
											<img class="w-100" src="assets/images/n5.png" alt="">
										</a>
										<h3><a href="#" title="">HENDRERIT NISI VENENATIS</a></h3>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
												<div class="list-mxh">
													<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
													<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
													<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
												</div>
											</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="blog">
										<a class="overflow" href="#" title="">
											<img class="w-100" src="assets/images/n3.png" alt="">
										</a>
										<h3><a href="#" title="">HENDRERIT NISI VENENATIS</a></h3>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
												<div class="list-mxh">
													<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
													<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
													<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<?php @include "sidebar.php"; ?>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
include "footer.php";
?>