
<?php
include "library/header-css.php";
?>
<header>
	<section class="header">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-3 col-4">
					<div class="header_logo">
						<a href="#" title="">
							<img src="assets/images/logo.png" alt="">
						</a>
					</div>
				</div>
				<div class="col-md-9 col-8">
					<div class="header_menu-login">
						<nav class="nav">
							<ul>
								<li><a href="#" title="">Giới thiệu</a></li>
								<li>
									<a href="#" title="">Kỹ năng <i class="fa fa-caret-down" aria-hidden="true"></i></a>
									<ul>
										<li><a href="#">kỹ năng mềm</a></li>
										<li><a href="#">kỹ năng nghề</a></li>
										<li><a href="#">Kỹ năng khác</a></li>
									</ul>
								</li>
								<li>
									<a href="#" title="">Diễn đàn <i class="fa fa-caret-down" aria-hidden="true"></i></a>
									<ul>
										<li><a href="#">Hợp đồng lao động</a></li>
										<li><a href="#">Bảo hiểm xã hội</a></li>
										<li><a href="#">An toàn lao động</a></li>
										<li><a href="#">Quy định khác</a></li>
									</ul>
								</li>
								<li>
									<a href="#" title="">Quy định pháp luật <i class="fa fa-caret-down" aria-hidden="true"></i></a>
									<ul>
										<li><a href="#">Chính sách dành cho người giúp việc gia đình </a></li>
										<li><a href="#">Người tìm việc</a></li>
										<li><a href="#">Việc tìm người</a></li>
										<li><a href="#">Giải đáp thắc mắc</a></li>
									</ul>
								</li>
							</ul>
						</nav>
						<div class="login">
							<div class="search-mobile">
								<a href="javascript:;">
									<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
									<g>
										<g>
											<path d="M508.875,493.792L353.089,338.005c32.358-35.927,52.245-83.296,52.245-135.339C405.333,90.917,314.417,0,202.667,0
											S0,90.917,0,202.667s90.917,202.667,202.667,202.667c52.043,0,99.411-19.887,135.339-52.245l155.786,155.786
											c2.083,2.083,4.813,3.125,7.542,3.125c2.729,0,5.458-1.042,7.542-3.125C513.042,504.708,513.042,497.958,508.875,493.792z
											M202.667,384c-99.979,0-181.333-81.344-181.333-181.333S102.688,21.333,202.667,21.333S384,102.677,384,202.667
											S302.646,384,202.667,384z"/>
										</g>
									</g>
								</svg>
								</a>
							</div>
							<div class="icon-mobile toggle hc-nav-trigger hc-nav-1">
								<a href="javascript:;"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
									<g>
										<g>
											<path d="M501.333,96H10.667C4.779,96,0,100.779,0,106.667s4.779,10.667,10.667,10.667h490.667c5.888,0,10.667-4.779,10.667-10.667
											S507.221,96,501.333,96z"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M501.333,245.333H10.667C4.779,245.333,0,250.112,0,256s4.779,10.667,10.667,10.667h490.667
											c5.888,0,10.667-4.779,10.667-10.667S507.221,245.333,501.333,245.333z"/>
										</g>
									</g>
									<g>
										<g>
											<path d="M501.333,394.667H10.667C4.779,394.667,0,399.445,0,405.333C0,411.221,4.779,416,10.667,416h490.667
											c5.888,0,10.667-4.779,10.667-10.667C512,399.445,507.221,394.667,501.333,394.667z"/>
										</g>
									</g>
								</svg></a>
							</div>
							<a href="#" title="">
								<img src="assets/images/icon/login.png" alt="">
								<span>Đăng nhập/ Đăng ký</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</header>