


<footer>
	<section class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<div class="row">
						<div class="col-md-6">
							<div class="footer_address">
								<h3 class="title">Website độc quyền tại</h3>
								<ul>
									<li>CÔNG TY CỔ PHẦN CHAPCANHUOCMO</li>
									<li>ĐKKD số: 0314635748</li>
									<li>Do Sở KH&ĐT Tp. Đà Nẵng Cấp ngày 21/09/2017</li>
								</ul>
							</div>
						</div>
						<div class="col-md-3">
							<div class="footer_menu">
								<h3 class="title">Website độc quyền tại</h3>
								<ul>
									<li><a href="#">Văn hóa</a></li>
									<li><a href="#">Nhu cầu tuyển dụng</a></li>
									<li><a href="#">Quy định pháp luật</a></li>
									<li><a href="#">Ứng xử tại nơi làm việc</a></li>
									<li><a href="#">Tin tức</a></li>
									<li><a href="#">Liên hệ</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-3">
							<div class="footer_menu">
								<h3 class="title">Thông Tin Khác</h3>
								<ul>
									<li><a href="#">Giới thiệu về GFCD</a></li>
									<li><a href="#">Giới thiệu về Rosa</a></li>
									<li><a href="#">Giới thiệu về dự án</a></li>
									<li><a href="#">Liên hệ</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-6">
							<div class="footer_address footer_address_custom">
								<h3 class="title">Trụ sở chính</h3>
								<ul>
									<li>Địa chỉ: Phòng 1504, Chung cư Bộ Khoa học và Công nghệ, tổ 22, phường Quan Hoa, quận Cầu Giấy, Hà Nội</li>
									<li>Email: gfcd08@gmail.com</li>
									<li>Hotline: (+84)2466837799</li>
								</ul>
							</div>
						</div>
						<div class="col-md-3">
							<div class="footer_connect">
								<h3 class="title">Kết nối với chúng tôi</h3>
								<div class="mxh">
									<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
									<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
									<a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a>
									<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="img">
								<img src="assets/images/bct.png" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="dowload">
						<img src="assets/images/dowload.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
</footer>
<section>
	<!-- Search -->
	<div class="full-search">
		<div class="input-search">
			<input type="text" placeholder="Tìm kiếm">
			<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
		</div>
		<div class="times-search">
			<a href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i></a>
		</div>
	</div>
	<!-- End Search -->

	<!-- SCROLL -->
	<div class="scroll-top">
		<a href="javascript:;"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
	</div>

	<!-- Menu-Mobile -->
	<div class="wrapper cf">
		<nav id="main-nav">
			<ul>
				<li><a href="#">Trang chủ</a></li>
				<li><a href="#" title="">Giới thiệu</a></li>
				<li>
					<a href="#" title="">Kỹ năng</a>
					<ul>
						<li><a href="#">kỹ năng mềm</a></li>
						<li><a href="#">kỹ năng nghề</a></li>
						<li><a href="#">Kỹ năng khác</a></li>
					</ul>
				</li>
				<li>
					<a href="#" title="">Diễn đàn</a>
					<ul>
						<li><a href="#">Hợp đồng lao động</a></li>
						<li><a href="#">Bảo hiểm xã hội</a></li>
						<li><a href="#">An toàn lao động</a></li>
						<li><a href="#">Quy định khác</a></li>
					</ul>
				</li>
				<li>
					<a href="#" title="">Quy định pháp luật</a>
					<ul>
						<li><a href="#">Chính sách dành cho người giúp việc gia đình </a></li>
						<li><a href="#">Người tìm việc</a></li>
						<li><a href="#">Việc tìm người</a></li>
						<li><a href="#">Giải đáp thắc mắc</a></li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</section>
<?php
include "library/footer-js.php";
?>