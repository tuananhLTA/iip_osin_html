


<section class="site-sidebar">
	<div class="site-sidebar_partner mb-bottom">
		<h2 class="heading"><a href="#" title="">Đối tác</a></h2>
		<div class="site-sidebar_partner_list">
			<div class="parter">
				<div class="row m-0">
					<div class="col-xl-3 col-4 p-0">
						<div class="img">
							<a href="#" title=""><img class="w-100" src="assets/images/dt1.png" alt=""></a>
						</div>
					</div>
					<div class="col-xl-9 col-8 p-0">
						<div class="text">
							<h3><a href="#" title="">Trung tâm Nghiên cứu Giới, Gia đình và 
							Phát triển Cộng đồng</a></h3>
						</div>
					</div>
				</div>
			</div>
			<div class="parter">
				<div class="row m-0">
					<div class="col-xl-3 col-4 p-0">
						<div class="img">
							<a href="#" title=""><img class="w-100" src="assets/images/g2.png" alt=""></a>
						</div>
					</div>
					<div class="col-xl-9 col-8 p-0">
						<div class="text">
							<h3><a href="#" title="">Trung tâm Nghiên cứu Giới, Gia đình và 
							Phát triển Cộng đồng</a></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="site-sidebar_video mb-bottom">
		<h2 class="heading"><a href="#" title="">Video</a></h2>
		<div class="site-sidebar_video_list">
			<div class="video">
				<img class="w-100 img-top" src="assets/images/g1.png" alt="">
				<a data-fancybox class="play-sidebar" href="assets/images/video/video.mp4" title="">
					<img src="assets/images/icon/play.png" alt="">
				</a>
			</div>
			<div class="video">
				<img class="w-100 img-top" src="assets/images/g2.png" alt="">
				<a data-fancybox class="play-sidebar" href="assets/images/video/video.mp4" title="">
					<img src="assets/images/icon/play.png" alt="">
				</a>
			</div>
			<div class="video">
				<img class="w-100 img-top" src="assets/images/g3.png" alt="">
				<a data-fancybox class="play-sidebar" href="assets/images/video/video.mp4" title="">
					<img src="assets/images/icon/play.png" alt="">
				</a>
			</div>
		</div>
	</div>
	<div class="site-sidebar_advertisement mb-bottom">
		<h2 class="heading"><a href="#" title="">Quảng cáo</a></h2>
		<div class="advertisement">
			<a href="#" title="">
				<img class="w-100" src="assets/images/qc2.png" alt="">
			</a>
		</div>
		<div class="advertisement">
			<a href="#" title="">
				<img class="w-100" src="assets/images/qc1.png" alt="">
			</a>
		</div>
	</div>
	<div class="site-sidebar_information">
		<h2 class="heading"><a href="#" title="">Thông tin truy cập</a></h2>
		<ul>
			<li>
				<span>Thành viên</span>
				<span class="number">154</span>
			</li>
			<li>
				<span>Số lượng truy cập</span>
				<span class="number">154</span>
			</li>
		</ul>
	</div>
</section>