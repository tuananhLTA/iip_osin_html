
<?php
include "header.php";
?>

<main class="main-page">
	<section class="site-post-details">
		<div class="container">
			<div class="back-post">
				<a href="#" title=""><i class="fa fa-arrow-left" aria-hidden="true"></i> Bài viết</a>
			</div>
			<div class="site-post-details_content">
				<div class="row">
					<div class="col-lg-8">
						<div class="post-details_content_left">
							<div class="news">
								<div class="user">
									<a href="#" title="">
										<img src="assets/images/user2.png" alt="">
									</a>
									<div class="user_name">
										<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
										<div class="date-times">
											<span class="times">17 phút</span>
											<span class="date">12/04/2029</span>
										</div>
									</div>
								</div>
								<div class="status">
									<div class="description">
										<p><strong>Mẹ ơi..! Con đói.</strong></p>
										<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng... Nhưng đó chỉ là những câu chuyện ngày xưa, ở một xã hội thực tại chúng ta sẽ bắt gặp những mảnh đời với cuộc sống ăn không đủ no, mặc không đủ ấm, mạng sống lay lắt như chầu chực và sẵn sàng ập xuống bất kỳ ai.</p>
										<p>Nếu có thể xin hãy tặng nhau bông hồng được không?</p>
										<p>Cách đây không lâu, tôi vô tình thấy một hình ảnh được chía sẻ rất nhiều trên mạng. Bức ảnh không quá đẹp về bối cảnh hay màu sắc nhưng ý nghĩa mà nó đọng lại trong trái tim mỗi người lại nhiều hơn những bức ảnh khác.</p>
									</div>
									<div class="list_icon">
										<a href="#" title="">
											<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
										</a>
									</div>
								</div>
							</div>
							<div class="comment">
								<div class="comment_form">
									<div class="img">
										<a href="#" title="">
											<img src="assets/images/user2.png" alt="">
										</a>
									</div>
									<div class="form">
										<form action="">
											<textarea class="form-control form-group" placeholder="Viết bình luận" rows="5" name=""></textarea>
											<button class="btn-primary btn" type="submit">Đăng bình luận</button>
										</form>
									</div>
								</div>
								<div class="comment_content">
									<span class="comment_number">78 Bình luận</span>
									<div class="comment_content_list">
										<div class="comment_content_list_item">
											<div class="item">
												<div class="user">
													<div class="img">
														<a href="#" title="">
															<img src="assets/images/user2.png" alt="">
														</a>
													</div>
													<div class="text">
														<span class="name"><a href="#" title="">Saliang</a></span>
														<p class="description m-0">Nice comic! I love it!!!</p>
													</div>
												</div>
												<div class="date">
													<ul>
														<li class="date">May 24, 2019</li>
														<li class="like">
															<span>156</span>
															<a href="#" title="">Thích</a>
														</li>
														<li class="rep">
															<span>78</span>
															<a href="#" title="">Trả lời</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="comment_content_list_item">
											<div class="item">
												<div class="user">
													<div class="img">
														<a href="#" title="">
															<img src="assets/images/user2.png" alt="">
														</a>
													</div>
													<div class="text">
														<span class="name"><a href="#" title="">Saliang</a></span>
														<p class="description m-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
													</div>
												</div>
												<div class="date">
													<ul>
														<li class="date">May 24, 2019</li>
														<li class="like">
															<span>156</span>
															<a href="#" title="">Thích</a>
														</li>
														<li class="rep">
															<span>78</span>
															<a href="#" title="">Trả lời</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="comment_content_list_item child_comment">
												<div class="user">
													<div class="img">
														<a href="#" title="">
															<img src="assets/images/user2.png" alt="">
														</a>
													</div>
													<div class="text">
														<span class="name"><a href="#" title="">Saliang</a></span>
														<p class="description m-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
													</div>
												</div>
												<div class="date">
													<ul>
														<li class="date">May 24, 2019</li>
														<li class="like">
															<span>156</span>
															<a href="#" title="">Thích</a>
														</li>
														<li class="rep">
															<span>78</span>
															<a href="#" title="">Trả lời</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="post-details_sidebar">
							<div class="post-details_sidebar_item">
								<h2 class="heading">Chia sẻ hàng đâu</h2>
								<div class="news">
									<div class="user">
										<a href="#" title="">
											<img src="assets/images/user2.png" alt="">
										</a>
										<div class="user_name">
											<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
											<div class="date-times">
												<span class="times">17 phút</span>
												<span class="date">12/04/2029</span>
											</div>
										</div>
									</div>
									<div class="status">
										<div class="description">
											<p><strong>Mẹ ơi..! Con đói.</strong></p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn..</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
								</div>
								<div class="news">
									<div class="user">
										<a href="#" title="">
											<img src="assets/images/user2.png" alt="">
										</a>
										<div class="user_name">
											<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
											<div class="date-times">
												<span class="times">17 phút</span>
												<span class="date">12/04/2029</span>
											</div>
										</div>
									</div>
									<div class="status">
										<div class="description">
											<p><strong>Mẹ ơi..! Con đói.</strong></p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn..</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
								</div>
								<div class="news">
									<div class="user">
										<a href="#" title="">
											<img src="assets/images/user2.png" alt="">
										</a>
										<div class="user_name">
											<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
											<div class="date-times">
												<span class="times">17 phút</span>
												<span class="date">12/04/2029</span>
											</div>
										</div>
									</div>
									<div class="status">
										<div class="description">
											<p><strong>Mẹ ơi..! Con đói.</strong></p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn..</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
								</div>
								<a class="more" href="#" title="">Xem thêm</a>
							</div>
						</div>
						<div class="post-details_sidebar">
							<div class="post-details_sidebar_item">
								<h2 class="heading">Bài đăng gần đây</h2>
								<div class="news">
									<div class="user">
										<a href="#" title="">
											<img src="assets/images/user2.png" alt="">
										</a>
										<div class="user_name">
											<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
											<div class="date-times">
												<span class="times">17 phút</span>
												<span class="date">12/04/2029</span>
											</div>
										</div>
									</div>
									<div class="status">
										<div class="description">
											<p><strong>Mẹ ơi..! Con đói.</strong></p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn..</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
								</div>
								<div class="news">
									<div class="user">
										<a href="#" title="">
											<img src="assets/images/user2.png" alt="">
										</a>
										<div class="user_name">
											<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
											<div class="date-times">
												<span class="times">17 phút</span>
												<span class="date">12/04/2029</span>
											</div>
										</div>
									</div>
									<div class="status">
										<div class="description">
											<p><strong>Mẹ ơi..! Con đói.</strong></p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn..</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
								</div>
								<div class="news">
									<div class="user">
										<a href="#" title="">
											<img src="assets/images/user2.png" alt="">
										</a>
										<div class="user_name">
											<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
											<div class="date-times">
												<span class="times">17 phút</span>
												<span class="date">12/04/2029</span>
											</div>
										</div>
									</div>
									<div class="status">
										<div class="description">
											<p><strong>Mẹ ơi..! Con đói.</strong></p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn..</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
								</div>
								<a class="more" href="#" title="">Xem thêm</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
include "footer.php";
?>