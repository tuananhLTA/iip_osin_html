
<?php
include "header.php";
?>

<main>
	<section class="site-news-page">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="site-news-page_content">
						<div class="the_content">
							<h1>HENDRERIT NISI VENENATIS</h1>
							<p><img class="w-100" src="assets/images/qc1.png" alt=""></p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							<p><img class="w-100" src="assets/images/qc2.png" alt=""></p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<?php @include "sidebar.php"; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="site-post-relateto">
		<div class="container">
			<div class="title-page text-center">
				<h2>BÀI VIẾT LIÊN QUAN</h2>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="blog">
						<a class="overflow" href="#" title="">
							<img class="w-100" src="assets/images/n1.png" alt="">
						</a>
						<h3><a href="#" title="">HENDRERIT NISI VENENATIS</a></h3>
						<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="blog">
						<a class="overflow" href="#" title="">
							<img class="w-100" src="assets/images/n2.png" alt="">
						</a>
						<h3><a href="#" title="">HENDRERIT NISI VENENATIS</a></h3>
						<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="blog">
						<a class="overflow" href="#" title="">
							<img class="w-100" src="assets/images/n3.png" alt="">
						</a>
						<h3><a href="#" title="">HENDRERIT NISI VENENATIS</a></h3>
						<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
include "footer.php";
?>