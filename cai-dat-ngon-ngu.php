
<?php
include "header.php";
?>

<main class="main-page">
	<section class="personal-information">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="sidebar_information">
						<div class="user">
							<img src="assets/images/user3.png" alt="">
							<span class="d-block">Nguyễn Hồng Thái</span>
						</div>
						<div class="list_function">
							<ul>
								<li><a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Thông tin cá nhân</a></li>
								<li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Cài đặt </a></li>
								<li><a href="#"><i class="fa fa-lock" aria-hidden="true"></i> Đổi mật khẩu</a></li>
								<li><a href="#"><i class="fa fa-sign-in" aria-hidden="true"></i> Đăng xuất</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="form_information -length">
						<h3>Cài đặt ngôn ngữ</h3>
						<form>
							<div class="form-group">
								<label class="container-label2 active-check">Vietnamese
									<input type="radio" checked="">
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="form-group">
								<label class="container-label2">English
									<input type="radio">
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="form-group">
								<label class="container-label2">Japanese
									<input type="radio">
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="form-group">
								<label class="container-label2">German
									<input type="radio">
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="form-group">
								<label class="container-label2">Chinese
									<input type="radio">
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="form-group">
								<label class="container-label2">Taiwanese
									<input type="radio">
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="form-group">
								<label class="container-label2">Korean
									<input type="radio">
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Cập nhật</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
include "footer.php";
?>