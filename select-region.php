<?php
    include "library/header-css.php";
?>

<section class="registration" style="background-image: url(assets/images/bg-dk.png);">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-xl-8 col-lg-7">
				<div class="registration_content">
					<div class="list-img">
						<a href="#" title="">
							<img src="assets/images/lg1.png" alt="">
						</a>
						<a href="#" title="">
							<img src="assets/images/lg2.png" alt="">
						</a>
						<a href="#" title="">
							<img src="assets/images/lg3.png" alt="">
						</a>
					</div>
					<h1>Chào mùng bạn <br>
					đến với trang thông tin dành cho người lao động</h1>
				</div>
			</div>
			<div class="col-xl-4 col-lg-5">
				<div class="registration_form select-region">
					<h2>Vui lòng chọn khu vực</h2>
					<form class="apply-form" action="">
						<div class="form-group">
							<label class="btn">Lao động Giúp việc gia đình <input class="check-radio" type="radio" name=""></label>
						</div>
						<div class="form-group m-0">
							<label class="btn">Lao động đi làm việc tại nước ngoài <input class="check-radio" type="radio" name=""></label>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="language">
		<a href="#">English</a>
		<a href="#">Vietnamese</a>
		<a href="#">Japanese</a>
		<a href="#">German</a>
		<a href="#">Chinese</a>
		<a href="#">Taiwanese</a>
		<a href="#">Korean</a>
	</div>
</section>




<?php
    include "library/footer-js.php";
?>