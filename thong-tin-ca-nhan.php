
<?php
include "header.php";
?>

<main class="main-page">
	<section class="personal-information">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="sidebar_information">
						<div class="user">
							<img src="assets/images/user3.png" alt="">
							<span class="d-block">Nguyễn Hồng Thái</span>
						</div>
						<div class="list_function">
							<ul>
								<li><a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Thông tin cá nhân</a></li>
								<li><a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Cài đặt </a></li>
								<li><a href="#"><i class="fa fa-lock" aria-hidden="true"></i> Đổi mật khẩu</a></li>
								<li><a href="#"><i class="fa fa-sign-in" aria-hidden="true"></i> Đăng xuất</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="form_information">
						<h3>Thông tin tài khoản</h3>
						<form>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-4">
										<label>Họ tên</label>
									</div>
									<div class="col-lg-9 col-md-8">
										<input type="text" class="form-control" placeholder="Họ và tên" name="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-4">
										<label>Avata</label>
									</div>
									<div class="col-lg-9 col-md-8">
										<div class="file-flex">
											<div class="list-img-upload">
												<div class="row row-custom">
													<div class="col-lg-3 col-md-4 col-6 col-custom">
														<img class="w-100" src="assets/images/n1.png" alt="">
													</div>
												</div>
											</div>
											<div class="file-upload btn btn-primary">
												<span>Chọn ảnh đại diện</span>
												<input type="file" class="file" name="">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-4">
										<label>Ngày sinh</label>
									</div>
									<div class="col-lg-9 col-md-8">
										<input type="text" class="form-control datepicker" placeholder="01/01/1995" name="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-4">
										<label>Email</label>
									</div>
									<div class="col-lg-9 col-md-8">
										<input type="text" class="form-control" placeholder="Example@email.com" name="">
										<a class="xt-email d-block text-right" href="#" title="">Xác thực email</a>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-4">
										<label>Số điện thoại</label>
									</div>
									<div class="col-lg-9 col-md-8">
										<input type="text" class="form-control" placeholder="+84" name="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-4">
										<label>Giới tính</label>
									</div>
									<div class="col-lg-9 col-md-8">
										<div class="row">
											<div class="col-lg-3 col-4">
												<label class="container-label2 active-check">Nam
													<input type="radio" checked="">
													<span class="checkmark"></span>
												</label>
											</div>
											<div class="col-lg-3 col-4">
												<label class="container-label2">Nữ
													<input type="radio">
													<span class="checkmark"></span>
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-4">
										<label>Tỉnh/Thành phố:</label>
									</div>
									<div class="col-lg-9 col-md-8">
										<select class="form-control">
											<option value="">Hà nội</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-4">
										<label>Quận/Huyện:</label>
									</div>
									<div class="col-lg-9 col-md-8">
										<select class="form-control">
											<option value="">Đống Đa</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-lg-3 col-md-4">
									</div>
									<div class="col-lg-9 col-md-8">
										<button class="btn btn-primary" type="submit">Cập nhật</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
include "footer.php";
?>