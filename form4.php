
<?php
include "header.php";
?>

<main class="main-page">
	<section class="site-form-page">
		<div class="container">
			<div class="site-form2_title">
				<div class="row">
					<div class="col-md-4">
						<div class="title">
							<h1 class="heading"><a href="#" title="">Forum chia sẻ</a></h1>
						</div>
					</div>
					<div class="col-md-8">
						<div class="search-form">
							<form action="">
								<div class="row justify-content-end">
									<div class="col-lg-3 col-md-4 col-5">
										<select class="form-control" name="" id="">
											<option value="">Mới nhất</option>
										</select>
									</div>
									<div class="col-md-6 col-7">
										<div class="relative">
											<input class="form-control" type="text" placeholder="Tìm kiếm" name="">
											<button class="btn" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="site-form2_content">
				<div class="row">
					<div class="col-md-4">
						<div class="category">
							<ul>
								<li><a href="#">Chính sách dành cho người giúp việc gia đình </a></li>
								<li><a href="#">Người tìm việc</a></li>
								<li><a href="#">Việc tìm người </a></li>
								<li><a class="active" href="#">Giải đáp thắc mắc </a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-8">
						<div class="content">
							<form class="form4-content" action="">
								<div class="form-group">
									<label>Họ và tên </label>
									<input class="form-control" type="text" name="">
								</div>
								<div class="form-group">
									<label>Hỏi đáp</label>
									<input class="form-control" type="text" name="">
								</div>
								<div class="form-group">
									<label>Nội dung thắc mắc </label>
									<textarea class="form-control" rows="5" name=""></textarea>
								</div>
								<div class="form-group">
									<label>Upload ảnh và video</label>
									<div class="list-upload">
										<div class="col-md-custom">
											<div class="upload_content">
												<img class="w-100" src="assets/images/qc1.png" alt="">
											</div>
										</div>
										<div class="col-md-custom">
											<div class="upload_content">
												<i class="fa fa-plus" aria-hidden="true"></i>
												<input class="file" type="file" name="">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group text-right">
									<button class="btn-gray btn" type="submit">Hủy</button>
									<button class="btn-primary btn" type="submit">Đăng bài</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
include "footer.php";
?>