
<?php
include "header.php";
?>

<main>
	<section class="site-slide">
		<div class="owl-theme sl-slide owl-carousel">
			<div class="item" style="background-image: url(assets/images/banner1.png);">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<div class="item_content">
								<h2 class="title">Giúp việc gia đình công nghệ 4.0</h2>
								<p>Giúp viện thời đại số? Tiếp cận cộng việc trên nhiều quốc gia, vùng lãnh thổ là khó? Hãy tham gia ngay cùng chúng tôi để biến điều đó thành hiện thực</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item" style="background-image: url(assets/images/banner1.png);">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<div class="item_content">
								<h2 class="title">Giúp việc gia đình công nghệ 4.0</h2>
								<p>Giúp viện thời đại số? Tiếp cận cộng việc trên nhiều quốc gia, vùng lãnh thổ là khó? Hãy tham gia ngay cùng chúng tôi để biến điều đó thành hiện thực</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item" style="background-image: url(assets/images/banner1.png);">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<div class="item_content">
								<h2 class="title">Giúp việc gia đình công nghệ 4.0</h2>
								<p>Giúp viện thời đại số? Tiếp cận cộng việc trên nhiều quốc gia, vùng lãnh thổ là khó? Hãy tham gia ngay cùng chúng tôi để biến điều đó thành hiện thực</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="site-search">
			<form>
				<select class="form-control selecter" name="">
					<option value="">Lựa chọn</option>
				</select>
				<input class="form-control" type="text" placeholder="Tìm kiếm" name="">
				<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
		</div>
	</section>
	<section class="site-home">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="site-home_content">
						<div class="site-home_content_item">
							<div class="title">
								<h1 class="heading"><a href="#" title="">Việc tìm người</a></h1>
								<a class="more" href="#" title="">Xem thêm</a>
							</div>
							<div class="news">
								<div class="user">
									<a href="#" title="">
										<img src="assets/images/user1.png" alt="">
									</a>
									<div class="user_name">
										<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
										<div class="date-times">
											<span class="times">17 phút</span>
											<span class="date">12/04/2029</span>
										</div>
									</div>
								</div>
								<div class="status">
									<div class="description">
										<p>Kỷ niệm 1 ngày tại phố đi bộ Seoul</p>
									</div>
									<div class="startus_images">
										<div class="row row-custom">
											<div class="col-md-6 col-custom">
												<div class="startus_img">
													<a href=# title="">
														<img class="w-100" src="assets/images/g2.png" alt="">
													</a>
													<a class="play" href="javascript:void(0);" title="">
														<img src="assets/images/icon/play.png" alt="">
													</a>
													<a class="pause" href="javascript:void(0);" title="">
														<i class="fa fa-pause" aria-hidden="true"></i>
													</a>
													<div class="video">
														<video class="video-child" controls>
															<source src="assets/images/video/video.mp4" type="video/mp4">
															<source src="assets/images/video/video.ogg" type="video/ogg">
														</video>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-custom">
												<div class="startus_img">
													<a href="#" title="">
														<img class="w-100" src="assets/images/g1.png" alt="">
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="list_news">
								<div class="owl-theme sl-news owl-carousel">
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="site-home_content_item">
							<div class="title">
								<h1 class="heading"><a href="#" title="">Người tìm việc </a></h1>
								<a class="more" href="#" title="">Xem thêm</a>
							</div>
							<div class="news">
								<div class="user">
									<a href="#" title="">
										<img src="assets/images/user2.png" alt="">
									</a>
									<div class="user_name">
										<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
										<div class="date-times">
											<span class="times">17 phút</span>
											<span class="date">12/04/2029</span>
										</div>
									</div>
								</div>
								<div class="status">
									<div class="description">
										<p><strong>Mẹ ơi..! Con đói.</strong></p>
										<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
									</div>
									<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
								</div>
							</div>
							<div class="list_news">
								<div class="owl-theme sl-news owl-carousel">
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="site-home_content_item">
							<div class="title">
								<h1 class="heading"><a href="#" title="">Chính sách dành cho người giúp việc gia đình </a></h1>
								<a class="more" href="#" title="">Xem thêm</a>
							</div>
							<div class="news">
								<div class="user">
									<a href="#" title="">
										<img src="assets/images/user2.png" alt="">
									</a>
									<div class="user_name">
										<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
										<div class="date-times">
											<span class="times">17 phút</span>
											<span class="date">12/04/2029</span>
										</div>
									</div>
								</div>
								<div class="status">
									<div class="description">
										<p><strong>Mẹ ơi..! Con đói.</strong></p>
										<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
									</div>
									<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
								</div>
							</div>
							<div class="list_news">
								<div class="owl-theme sl-news owl-carousel">
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
									<div class="list_news_item">
										<div class="user">
											<a href="#" title="">
												<img src="assets/images/user2.png" alt="">
											</a>
											<div class="user_name">
												<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
												<div class="date-times">
													<span class="times">17 phút</span>
													<span class="date">12/04/2029</span>
												</div>
											</div>
										</div>
										<div class="description">
											<p>Mẹ ơi..! Con đói.</p>
											<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
										</div>
										<div class="list_icon">
											<a href="#" title="">
												<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
											</a>
											<a href="#" title="">
												<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<?php @include "sidebar.php"; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="site-search-word">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="content">
						<h3>TÌM KIẾM CÔNG VIỆC TỐT NHẤT</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
						<img src="assets/images/dowload.png" alt="">
					</div>
				</div>
				<div class="col-md-6">
					<div class="img">
						<img class="w-100" src="assets/images/iphone.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="site-support">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-lg-4 col-md-6">
					<div class="site-support_content">
						<h3>Chúng tôi luôn sẵn sàng 
						hỗ trợ bạn</h3>
						<p>Khi có bất kỳ thắc mắc gì về dịch vụ, bạn chỉ cần chạm nhẹ, chúng tôi sẽ giải đáp cho bạn</p>
						<ul>
							<li>Email: <a href="#">support@Example.com</a></li>
							<li>Hotline: <a href="#" title="">1900.636.736</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-5 col-md-6">
					<div class="site-support_form">
						<form action="">
							<div class="form-group">
								<input class="form-control" placeholder="Tên của bạn" type="text" name="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Số điện thoại" type="text" name="">
							</div>
							<div class="form-group">
								<textarea class="form-control" rows="3" placeholder="Nội dung" name=""></textarea>
							</div>
							<div class="text-right"><button class="btn" type="submit">Gửi thông tin</button></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
include "footer.php";
?>