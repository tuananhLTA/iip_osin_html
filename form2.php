
<?php
include "header.php";
?>

<main class="main-page">
	<section class="site-form-page">
		<div class="container">
			<div class="site-form2_title">
				<div class="row">
					<div class="col-md-4">
						<div class="title">
							<h1 class="heading"><a href="#" title="">Forum chia sẻ</a></h1>
						</div>
					</div>
					<div class="col-md-8">
						<div class="search-form">
							<form action="">
								<div class="row justify-content-end">
									<div class="col-lg-3 col-md-4 col-5">
										<select class="form-control" name="" id="">
											<option value="">Mới nhất</option>
										</select>
									</div>
									<div class="col-md-6 col-7">
										<div class="relative">
											<input class="form-control" type="text" placeholder="Tìm kiếm" name="">
											<button class="btn" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="site-form2_content">
				<div class="row">
					<div class="col-md-4">
						<div class="category">
							<ul>
								<li><a href="#">Chính sách dành cho người giúp việc gia đình </a></li>
								<li><a class="active" href="#">Người tìm việc</a></li>
								<li><a href="#">Việc tìm người </a></li>
								<li><a href="#">Giải đáp thắc mắc </a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-8">
						<div class="content">
							<div class="news">
								<div class="user">
									<a href="#" title="">
										<img src="assets/images/user2.png" alt="">
									</a>
									<div class="user_name">
										<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
										<div class="date-times">
											<span class="times">17 phút</span>
											<span class="date">12/04/2029</span>
										</div>
									</div>
								</div>
								<div class="status">
									<div class="description">
										<p><strong>Mẹ ơi..! Con đói.</strong></p>
										<p>Chiến tranh ập tới khiến cuộc sống mọi người đảo lộn, đặc biệt là con trẻ. Những đứa trẻ còn chưa biết cuộc đời vuông tròn đã phải nghe tiếng bom đạn thay vì tiếng ru, tiếng nựng...</p>
									</div>
									<div class="list_icon">
										<a href="#" title="">
											<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											<div class="list-mxh">
												<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
												<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
												<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="news">
								<div class="user">
									<a href="#" title="">
										<img src="assets/images/hi.png" alt="">
									</a>
									<div class="user_name">
										<h3 class="name"><a href="#" title="">Van Anh</a></h3>
										<div class="date-times">
											<span class="times">17 phút</span>
											<span class="date">12/04/2029</span>
										</div>
									</div>
								</div>
								<div class="status">
									<div class="description">
										<p>Kỷ niệm 1 ngày tại phố đi bộ Seoul</p>
									</div>
									<div class="startus_images">
										<div class="row row-custom">
											<div class="col-md-6 col-custom">
												<div class="startus_img">
													<a href="#" title="">
														<img class="w-100" src="assets/images/qc1.png" alt="">
													</a>
												</div>
											</div>
											<div class="col-md-6 col-custom">
												<div class="startus_img">
													<a href="#" title="">
														<img class="w-100" src="assets/images/qc2.png" alt="">
													</a>
												</div>
											</div>
										</div>
									</div>
									<div class="list_icon">
										<a href="#" title="">
											<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											<div class="list-mxh">
												<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
												<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
												<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="news">
								<div class="user">
									<a href="#" title="">
										<img src="assets/images/hi1.png" alt="">
									</a>
									<div class="user_name">
										<h3 class="name"><a href="#" title="">Thúy Kiều</a></h3>
										<div class="date-times">
											<span class="times">17 phút</span>
											<span class="date">12/04/2029</span>
										</div>
									</div>
								</div>
								<div class="status">
									<div class="description">
										<p>Thưởng thức bữa ăn tại nhà hàng Sasimi tại Nhật Bản thật tuyệt vời!</p>
									</div>
									<div class="startus_images">
										<div class="row row-custom">
											<div class="col-md-12 col-custom">
												<div class="startus_img">
													<a href="#" title="">
														<img class="w-100" src="assets/images/nb.png" alt="">
													</a>
													<a class="play" href="javascript:void(0);" title="">
														<img src="assets/images/icon/play.png" alt="">
													</a>
													<a class="pause" href="javascript:void(0);" title="">
														<i class="fa fa-pause" aria-hidden="true"></i>
													</a>
													<div class="video">
														<video class="video-child" controls="">
															<source src="assets/images/video/video.mp4" type="video/mp4">
															<source src="assets/images/video/video.ogg" type="video/ogg">
														</video>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="list_icon">
										<a href="#" title="">
											<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											<div class="list-mxh">
												<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
												<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
												<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
											</div>
										</a>
									</div>
								</div>
							</div>
							<div class="news">
								<div class="user">
									<a href="#" title="">
										<img src="assets/images/user2.png" alt="">
									</a>
									<div class="user_name">
										<h3 class="name"><a href="#" title="">Linh Linh</a></h3>
										<div class="date-times">
											<span class="times">17 phút</span>
											<span class="date">12/04/2029</span>
										</div>
									</div>
								</div>
								<div class="status">
									<div class="description">
										<p>Kỷ niệm 1 ngày tại phố đi bộ Seoul</p>
									</div>
									<div class="startus_images">
										<div class="row row-custom">
											<div class="col-md-6 col-custom">
												<div class="startus_img">
													<a href="#" title="">
														<img class="w-100" src="assets/images/nb.png" alt="">
													</a>
													<a class="play" href="javascript:void(0);" title="">
														<img src="assets/images/icon/play.png" alt="">
													</a>
													<a class="pause" href="javascript:void(0);" title="">
														<i class="fa fa-pause" aria-hidden="true"></i>
													</a>
													<div class="video">
														<video class="video-child" controls="">
															<source src="assets/images/video/video.mp4" type="video/mp4">
															<source src="assets/images/video/video.ogg" type="video/ogg">
														</video>
													</div>
												</div>
											</div>
											<div class="col-md-6 col-custom">
												<div class="startus_img">
													<a href="#" title="">
														<img class="w-100" src="assets/images/nb.png" alt="">
													</a>
												</div>
											</div>
										</div>
									</div>
									<div class="list_icon">
										<a href="#" title="">
											<span><i class="fa fa-heart-o" aria-hidden="true"></i> 156</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-comment-o" aria-hidden="true"></i> 78</span>
										</a>
										<a href="#" title="">
											<span><i class="fa fa-share" aria-hidden="true"></i> 10</span>
											<div class="list-mxh">
												<span><i class="fa fa-facebook" aria-hidden="true"></i></span>
												<span><i class="fa fa-twitter" aria-hidden="true"></i></span>
												<span><i class="fa fa-instagram" aria-hidden="true"></i></span>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php
include "footer.php";
?>